import pydicom
import os
import tkinter as tk
from tkinter import *
from tkinter import filedialog
from PIL import ImageTk, Image
import tkfilebrowser
import cv2

#Creating root widget for the desktop app
global im_or
root = Tk()
root.title('Chest X-Ray COVID Analysis Tool')


#Reading file from the desktop GUI
def open_file():
    root.update()
    root.filename = tkfilebrowser.askopenfilename(initialdir = "./",title= "Select a File", \
                                    filetypes = [("dcm files","*.dcm"),("all files","*.*")])


def cov_run():
    root.update()
    img_name, extn = os.path.splitext(os.path.basename(root.filename))

    os.system('python run_cov_app.py -imfile '+ root.filename)
    im_or = pydicom.dcmread(root.filename).pixel_array
    img_an = cv2.imread('op_masked_'+img_name+'.png')

    fig, axes = plt.subplots(1,2,figsize=(15,7))
    axes[0].imshow(im_or, cmap = 'gray')
    axes[1].imshow(im_an,cmap = 'gray')
    plt.gcf().canvas.set_window_title("analyzed_"+img_name)
    axes[0].axis('off')
    axes[1].axis('off')
    fig.suptitle(img_name)
    plt.show()

#Executing the processes on button click
def open_test():
    open_file()
    cov_run()

#Defining the button fields and config parameters
image_load_btn = Button(root, text = "COV Analysis", padx= 50,pady = 50,command = open_test)
image_load_btn.pack()

#Displaying app information on the Label in GUI
img_label = Label(root,text = "This tool detects COVID in CXRs.")


img_label.pack()
root.mainloop()
