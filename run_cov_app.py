import os
import matplotlib.pyplot as plt
import argparse
import cv2
import pydicom


os.system('mkdir test')
os.system('touch ./test/inf_scores')

parser = argparse.ArgumentParser(description = 'COVID CXR Analysis')
parser.add_argument('-imfile', help= 'Path to image')
parser.add_argument('-outfile', help= 'dir')

args = parser.parse_args()


dcm_name = args.imfile
ip_dcm = dcm_name

im = pydicom.dcmread(ip_dcm).pixel_array
fname = dcm_name.split('.')[0] +'.png'
ip_fname = './'+fname
print(fname)
cv2.imwrite(ip_fname,im)
fname_mask = './masked_'+fname

os.system('python inference.py \
    --weightspath models/COVIDNet-CXR4-A \
    --metaname model.meta \
    --ckptname model-18540 \
    --imagepath '+ip_fname)


os.system('python lung_args.py --img_dir '+ip_fname+' --visualize_dir ./')


src1 = cv2.imread(ip_fname)
src2 = cv2.imread('./api_seg_'+fname)
src2 = cv2.resize(src2, src1.shape[1::-1])

dst = cv2.bitwise_and(src1, src2)

cv2.imwrite('./test_mask_'+fname, dst)


s1 = cv2.imread(ip_fname)
s2 = cv2.imread("./test_mask_"+fname)

s2 = cv2.resize(s2, s1.shape[1::-1],cv2.INTER_AREA)

dst = cv2.addWeighted(s1, 0.1, s2, 0.9, 0)

cv2.imwrite('./masked_'+fname, dst)


os.system('python  ./COVID-Net-GradCAM.py \
            -model ./COVIDNet-CXR4-A.json \
            -impath '+fname_mask+'\
            -pred_class COVID-19 \
            -outdir ./ \
            -imgfile '+ip_fname)

os.system('rm *.png')
os.system('rm *.dcm')


#
# os.system('python  ./COVID-Net-GradCAM.py \
#             -model ./COVID-Net-4A.json \
#             -impath '+ip_fname+'\
#             -pred_class COVID-19 \
#             -outdir ./op_files \
#             -imgfile '+ip_fname)
