# web-app for API image manipulation

from flask import Flask, request, render_template, send_from_directory
import os
from PIL import Image
import pydicom
import cv2
from flask_ngrok import run_with_ngrok

app = Flask(__name__)

run_with_ngrok(app)


APP_ROOT = os.path.dirname(os.path.abspath(__file__))


# default access page
@app.route("/")
def main():
    return render_template('index.html')


# upload selected image and forward to processing page
@app.route("/upload", methods=["POST"])
def upload():
    target = os.path.join(APP_ROOT, 'static/images/')

    # create image directory if not found
    if not os.path.isdir(target):
        os.mkdir(target)

    # retrieve file from html file-picker
    upload = request.files.getlist("file")[0]
    print("File name: {}".format(upload.filename))
    filename = upload.filename

    # file support verification
    ext = os.path.splitext(filename)[1]
    if (ext == ".jpg") or (ext == ".png") or (ext == ".dcm"):
        print("File accepted")
    else:
        return render_template("error.html", message="The selected file is not supported"), 400

    print(filename)

    # save file
    destination = "/".join([target, filename])
    print("File saved to to:", destination)
    upload.save(destination)
    os.system('cp '+destination+' ./')
    os.system('python run_cov_app.py -imfile '+filename)
    filename = os.path.splitext(filename)[0] +'.png'
    im = pydicom.dcmread(destination).pixel_array
    img = Image.fromarray(im)
    destination = "/".join([target, filename])
    img.save(destination)
    filename = 'temp.png'
    os.system('mv ./static/images/temp.png ./')
    os.system('rm ./static/images/*.png ./static/images/*.dcm')
    os.system('mv ./temp.png ./static/images/')

    # forward to processing page
    return send_image('temp.png')

# retrieve file from 'static/images' directory
@app.route('/static/images/<filename>')
def send_image(filename):
    return send_from_directory("static/images", filename)


if __name__ == "__main__":
    app.run()
